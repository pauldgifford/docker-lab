
### Docker Lab Installer v1.0
![alt text](http://static1.squarespace.com/static/50203aca84ae954efd2f9b70/t/55dd50b3e4b087612e6d759e/1455081593314/?format=250w "cloudcanuck")
###### Author: Paul Gifford <pgifford@vmware.com>
###### Blog Post: http://www.cloudcanuck.ca/posts/docker-lab-installer

###### Automated setup for installing a Docker Lab on your Mac

###### Prerequisites:
  - OSX Yosemite and  El Capitan (not tested on the Beta or Developer Previews)
  - Terminal
  - Internet Access

###### Changelog:
  - 1.0 Initial Build

###### Install:
```
curl -O https://raw.githubusercontent.com/pauldgifford/appcatalystlab/master/docker-lab.sh
```

**Disclaimer: Use at your own risk, this has been tested on default installs of OS X Yosemite and El Capitan without any customizations.  This has not been tested on the Beta or Developer Previews from Apple.**
